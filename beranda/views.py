from django.shortcuts import render

# Create your views here.
def beranda(request):
	return render(request, 'beranda.html')

def tentang(request):
	return render(request, 'tentang.html')	

def proyek(request):
	return render(request, 'proyek.html')
